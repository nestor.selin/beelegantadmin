$(document).ready(function(){
    const form = $('.form');
    const sendFrom = $('#sendFrom');
    const clearForm = $('#clearForm');
    const successNotification = $('#successNotification');
    var input = $('.input');
    let formData = {};

    input.on('keyup', function(){
        $(this).removeClass('input--invalid');
    })

    sendFrom.on('click',function(event){
        event.preventDefault();
        formData = {};
        form.find(input).each(function(){
            if($(this).val()) {
                $(this).removeClass('input--invalid');
                let fieldName = $(this).attr('id');
                let fieldVal = $(this).val();
                let fieldData = {};
                fieldData[fieldName] = fieldVal;
                formData = {...formData, ...fieldData}
            } else {
                $(this).addClass('input--invalid');
                return false;
            }
        });
        if(input.length === Object.keys(formData).length) {
            sendData(formData);
        }
    });


    function sendData(formData) {
        let formUrl = form.attr('id');
        console.log(formData); // An Object for Sending to Backend
        $.ajax( {
            type: 'POST',
            url: `http://3.15.225.146:3000/${formUrl}`, // Url to Backend Method
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                successNotification.addClass('visible'); // If Data Successfully Sent Show this Notification
            },
            error: function (data) {
                console.log(data);
                let errorText = 'Something Wrong' // Change this value to Error Text from returned [Data] Object (data.statusText)
                if($('.error').length) {
                    $('.error').remove();
                }
                form.prepend('<div class="error">' + errorText + '</div>')
            }
        })
    }


    clearForm.on('click',function(){
        form.find(input).each(function(){
            $(this).val("");
        });
        successNotification.removeClass('visible');
    });

});